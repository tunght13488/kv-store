#!/usr/bin/env bash

set -Eeuo pipefail

# ARG CACHE_BUST
# RUN echo "${CACHE_BUST}" \
#     && apt-get update \
#     && apt-get install -y \
#     tree \
#     && php --version \
#     && php --ini \
#     && php -m \
#     && php -i | grep memory_limit \
#     && tree /usr/local/etc/php/ \
#     && echo " ==> DEBUGGING"

function build_base_docker() {
    cd docker/docker

    docker build \
        --target base-docker \
        --progress=plain \
        --tag $IMAGE_BASE_DOCKER:$TAG_COMMIT \
        --tag $REPO/$IMAGE_BASE_DOCKER:$TAG_COMMIT \
        --tag $IMAGE_BASE_DOCKER:$TAG_LATEST \
        --tag $REPO/$IMAGE_BASE_DOCKER:$TAG_LATEST \
        .

    docker push $REPO/$IMAGE_BASE_DOCKER:$TAG_COMMIT

    docker push $REPO/$IMAGE_BASE_DOCKER:$TAG_LATEST

    cd ../..
}

function build_base_php_cli() {
    cd docker/php

    docker build \
        --target base-php-cli \
        --build-arg TARGET_IMAGE_TYPE=cli \
        --progress=plain \
        --tag $IMAGE_BASE_PHP_CLI:$TAG_COMMIT \
        --tag $REPO/$IMAGE_BASE_PHP_CLI:$TAG_COMMIT \
        --tag $IMAGE_BASE_PHP_CLI:$TAG_LATEST \
        --tag $REPO/$IMAGE_BASE_PHP_CLI:$TAG_LATEST \
        .

    docker push $REPO/$IMAGE_BASE_PHP_CLI:$TAG_COMMIT
    docker push $REPO/$IMAGE_BASE_PHP_CLI:$TAG_LATEST

    cd ../..
}

function build_base_php_fpm() {
    cd docker/php

    docker build \
        --target base-php-fpm \
        --build-arg TARGET_IMAGE_TYPE=fpm \
        --progress=plain \
        --tag $IMAGE_BASE_PHP_FPM:$TAG_COMMIT \
        --tag $REPO/$IMAGE_BASE_PHP_FPM:$TAG_COMMIT \
        --tag $IMAGE_BASE_PHP_FPM:$TAG_LATEST \
        --tag $REPO/$IMAGE_BASE_PHP_FPM:$TAG_LATEST \
        .

    docker push $REPO/$IMAGE_BASE_PHP_FPM:$TAG_COMMIT
    docker push $REPO/$IMAGE_BASE_PHP_FPM:$TAG_LATEST

    cd ../..
}

function build_base_nginx() {
    cd docker/nginx

    docker build \
        --target base-nginx \
        --progress=plain \
        --tag $IMAGE_BASE_NGINX:$TAG_COMMIT \
        --tag $REPO/$IMAGE_BASE_NGINX:$TAG_COMMIT \
        --tag $IMAGE_BASE_NGINX:$TAG_LATEST \
        --tag $REPO/$IMAGE_BASE_NGINX:$TAG_LATEST \
        .

    docker push $REPO/$IMAGE_BASE_NGINX:$TAG_COMMIT
    docker push $REPO/$IMAGE_BASE_NGINX:$TAG_LATEST

    cd ../..
}

function build_laravel_code() {
    docker build \
        --target laravel-code \
        --build-arg BASE_PHP_CLI="$REPO/$IMAGE_BASE_PHP_CLI:$TAG_COMMIT" \
        --progress=plain \
        --tag $IMAGE_LARAVEL_CODE:$TAG_COMMIT \
        --tag $REPO/$IMAGE_LARAVEL_CODE:$TAG_COMMIT \
        --tag $IMAGE_LARAVEL_CODE:$TAG_LATEST \
        --tag $REPO/$IMAGE_LARAVEL_CODE:$TAG_LATEST \
        .

    docker push $REPO/$IMAGE_LARAVEL_CODE:$TAG_COMMIT
    docker push $REPO/$IMAGE_LARAVEL_CODE:$TAG_LATEST
}

function build_laravel_deploy() {
    cd docker/laravel

    docker build \
        --target laravel-deploy \
        --build-arg LARAVEL_CODE="$REPO/$IMAGE_LARAVEL_CODE:$TAG_COMMIT" \
        --progress=plain \
        --tag $IMAGE_LARAVEL_DEPLOY:$TAG_COMMIT \
        --tag $REPO/$IMAGE_LARAVEL_DEPLOY:$TAG_COMMIT \
        --tag $IMAGE_LARAVEL_DEPLOY:$TAG_LATEST \
        --tag $REPO/$IMAGE_LARAVEL_DEPLOY:$TAG_LATEST \
        .

    docker push $REPO/$IMAGE_LARAVEL_DEPLOY:$TAG_COMMIT
    docker push $REPO/$IMAGE_LARAVEL_DEPLOY:$TAG_LATEST

    cd ../..
}

function build_laravel_fpm() {
    cd docker/laravel

    docker build \
        --target laravel-fpm \
        --build-arg BASE_PHP_FPM="$REPO/$IMAGE_BASE_PHP_FPM:$TAG_LATEST" \
        --build-arg LARAVEL_CODE="$REPO/$IMAGE_LARAVEL_CODE:$TAG_COMMIT" \
        --progress=plain \
        --tag $IMAGE_LARAVEL_FPM:$TAG_COMMIT \
        --tag $REPO/$IMAGE_LARAVEL_FPM:$TAG_COMMIT \
        --tag $IMAGE_LARAVEL_FPM:$TAG_LATEST \
        --tag $REPO/$IMAGE_LARAVEL_FPM:$TAG_LATEST \
        .

    docker push $REPO/$IMAGE_LARAVEL_FPM:$TAG_COMMIT
    docker push $REPO/$IMAGE_LARAVEL_FPM:$TAG_LATEST

    cd ../..
}

function build_laravel_nginx() {
    cd docker/laravel

    docker build \
        --target laravel-nginx \
        --build-arg BASE_NGINX="$REPO/$IMAGE_BASE_NGINX:$TAG_LATEST" \
        --build-arg LARAVEL_CODE="$REPO/$IMAGE_LARAVEL_CODE:$TAG_COMMIT" \
        --progress=plain \
        --tag $IMAGE_LARAVEL_NGINX:$TAG_COMMIT \
        --tag $REPO/$IMAGE_LARAVEL_NGINX:$TAG_COMMIT \
        --tag $IMAGE_LARAVEL_NGINX:$TAG_LATEST \
        --tag $REPO/$IMAGE_LARAVEL_NGINX:$TAG_LATEST \
        .

    docker push $REPO/$IMAGE_LARAVEL_NGINX:$TAG_COMMIT
    docker push $REPO/$IMAGE_LARAVEL_NGINX:$TAG_LATEST

    cd ../..
}

function run_laravel_deploy() {
    docker run \
        -it \
        --rm \
        --name="kv_deploy" \
        --network="host" \
        --mount type=bind,source=/var/run/mysqld/mysqld.sock,target=/var/run/mysqld/mysqld.sock \
        --mount type=bind,source=$(pwd)/.env,target=/app/.env,readonly \
        $IMAGE_LARAVEL_DEPLOY
}

function run_laravel_fpm() {
    docker run \
        -it \
        --rm \
        --name="kv_fpm" \
        --network="host" \
        --mount type=bind,source=/var/run/mysqld/mysqld.sock,target=/var/run/mysqld/mysqld.sock \
        --mount type=bind,source=$(pwd)/.env,target=/app/.env,readonly \
        $IMAGE_LARAVEL_FPM:$TAG_LATEST
}

function run_laravel_nginx() {
    docker run \
        -it \
        --rm \
        --name="kv_nginx" \
        --network="host" \
        --mount type=bind,source=/var/run/mysqld/mysqld.sock,target=/var/run/mysqld/mysqld.sock \
        --mount type=bind,source=$(pwd)/.env,target=/app/.env,readonly \
        $IMAGE_LARAVEL_NGINX:$TAG_LATEST
}

function run_docker() {
    docker run \
        -it \
        --rm \
        --name="kv_docker" \
        --network="host" \
        $IMAGE_BASE_DOCKER:$TAG_LATEST \
        doctl auth init --access-token dop_v1_b01bebe445ca0e67602597e186aa36e9f0b3072c978cc9289a3efb9591e40de5 \
        && doctl account get
}

function docker_build() {
    # build_base_docker
    build_base_php_cli
    # build_base_php_fpm
    # build_base_nginx
    # build_laravel_code
    # build_laravel_deploy
    # build_laravel_fpm
    # build_laravel_nginx
}

function helm_plan() {
    cd helm/kv-store

    helm install \
        --dry-run \
        --debug \
        --namespace $NAMESPACE \
        --create-namespace \
        --values $VALUES_FILE \
        --set-string deploy.tag=$TAG_COMMIT \
        --set-string fpm.tag=$TAG_COMMIT \
        --set-string nginx.tag=$TAG_COMMIT \
        --set-string envSecrets.DOCKERCONFIGJSON=$DOCKERCONFIGJSON \
        --set-string envSecrets.APP_KEY=$APP_KEY \
        --set-string envSecrets.DB_PASSWORD=$DB_PASSWORD \
        $RELEASE_NAME \
        .

    cd ../..
}

function helm_install() {
    cd helm/kv-store

    helm install \
        --debug \
        --namespace $NAMESPACE \
        --create-namespace \
        --values $VALUES_FILE \
        --set-string deploy.tag=$TAG_COMMIT \
        --set-string fpm.tag=$TAG_COMMIT \
        --set-string nginx.tag=$TAG_COMMIT \
        --set-string envSecrets.DOCKERCONFIGJSON=$DOCKERCONFIGJSON \
        --set-string envSecrets.APP_KEY=$APP_KEY \
        --set-string envSecrets.DB_PASSWORD=$DB_PASSWORD \
        $RELEASE_NAME \
        .

    cd ../..
}

function helm_upgrade() {
    cd helm/kv-store

    helm upgrade \
        --debug \
        --namespace $NAMESPACE \
        --values $VALUES_FILE \
        --set-string deploy.tag=$TAG_COMMIT \
        --set-string fpm.tag=$TAG_COMMIT \
        --set-string nginx.tag=$TAG_COMMIT \
        --set-string envSecrets.DOCKERCONFIGJSON=$DOCKERCONFIGJSON \
        --set-string envSecrets.APP_KEY=$APP_KEY \
        --set-string envSecrets.DB_PASSWORD=$DB_PASSWORD \
        $RELEASE_NAME \
        .

    cd ../..
}

function helm_deploy() {
    export DEPLOYS=$(helm ls --namespace=$NAMESPACE | grep $RELEASE_NAME | grep -v nginx-ingress | wc -l)
    if [[ ${DEPLOYS}  -eq 0 ]]; then
        echo "helm_install"
        helm_install
    else
        echo "helm_upgrade"
        helm_upgrade
    fi
}

# Docker control
export DOCKER_BUILDKIT=1
# Docker repo
export REPO="registry.gitlab.com/tunght13488/kv-store"
# Docker images
export IMAGE_BASE_DOCKER="kv-base-docker"
export IMAGE_BASE_PHP_CLI="kv-base-php-cli"
export IMAGE_BASE_PHP_FPM="kv-base-php-fpm"
export IMAGE_BASE_NGINX="kv-base-nginx"
export IMAGE_LARAVEL_CODE="kv-laravel-code"
export IMAGE_LARAVEL_DEPLOY="kv-laravel-deploy"
export IMAGE_LARAVEL_FPM="kv-laravel-fpm"
export IMAGE_LARAVEL_NGINX="kv-laravel-nginx"
# Docker tags
export TAG_COMMIT="local-0.0.3"
export TAG_LATEST="local-latest"
# Helm control
export NAMESPACE=default
export RELEASE_NAME=kv-store
export VALUES_FILE=values.yaml
# Helm secrets
export DOCKERCONFIGJSON="ewogICAgImF1dGhzIjogewogICAgICAgICJodHRwczovL3JlZ2lzdHJ5LmdpdGxhYi5jb20iOiB7CiAgICAgICAgICAgICJhdXRoIjogImRIVnVaMmgwTVRNME9EZzZaMnh3WVhRdFpYQnRjMWRITnpKNldVRk5ZM0ZWTFVvNFkwND0iCiAgICAgICAgfQogICAgfQp9Cg=="
export APP_KEY="base64:0EvFNFt7PR7soeUtrQn5Lww161YMB4e/Ab/ZmbZwjhI="
export DB_PASSWORD='Admin!23'

# helm_plan
docker_build
# run_laravel_deploy
# run_laravel_fpm
# run_laravel_nginx
# run_docker
# helm_deploy
