<?php

namespace App\Providers;

use App\Service\HashManagerInterface;
use App\Service\RedisHashManager;
use App\Service\RedisWithEloquentFailoverHashManager;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        // $this->app->bind(HashManagerInterface::class, RedisHashManager::class);
        $this->app->bind(HashManagerInterface::class, RedisWithEloquentFailoverHashManager::class);
    }

    public function boot(): void
    {
    }
}
