<?php

declare(strict_types=1);

namespace App\Service;

use App\Models\Hash;
use Carbon\CarbonImmutable;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Support\Collection;
use Throwable;

class EloquentHashManager implements HashManagerInterface
{
    /**
     * @param string $key
     * @param mixed $value
     * @return bool
     * @throws Throwable
     */
    public function create(string $key, mixed $value): bool
    {
        $hash = new Hash();
        $hash->identifier = $key;
        $hash->content = $value;
        $hash->timestamp = CarbonImmutable::now()->timestamp;
        $hash->saveOrFail();
        return true;
    }

    public function get(string $key, ?int $timestamp): mixed
    {
        $hash = Hash::ofTimestamp($timestamp)
            ->where('identifier', $key)
            ->firstOrFail();
        return $hash->content;
    }

    public function getAll(): Collection
    {
        return Hash::query()
            ->select('h.*')
            ->from('hashes', 'h')
            ->leftJoin('hashes AS tmp', function (JoinClause $join) {
                $join->on('h.identifier', '=', 'tmp.identifier')
                    ->on('h.timestamp', '<', 'tmp.timestamp');
            })
            ->whereNull('tmp.timestamp')
            ->orderByDesc('timestamp')
            ->get()
            ->mapWithKeys(function (Hash $hash) {
                return [$hash->identifier => $hash->content];
            });
    }
}
