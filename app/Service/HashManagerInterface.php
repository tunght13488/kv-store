<?php

declare(strict_types=1);

namespace App\Service;

use Illuminate\Support\Collection;

interface HashManagerInterface
{
    public function create(string $key, mixed $value): bool;

    public function get(string $key, ?int $timestamp): mixed;
    
    public function getAll(): Collection;
}
