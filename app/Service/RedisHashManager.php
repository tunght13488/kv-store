<?php

declare(strict_types=1);

namespace App\Service;

use Carbon\CarbonImmutable;
use Illuminate\Contracts\Redis\Factory;
use Illuminate\Redis\Connections\Connection;
use Illuminate\Redis\Connections\PhpRedisConnection;
use Illuminate\Support\Collection;
use JsonException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

class RedisHashManager implements HashManagerInterface
{
    /**
     * @var Connection&PhpRedisConnection
     */
    private Connection $redis;

    public function __construct(Factory $factory)
    {
        $this->redis = $factory->connection();
    }

    /**
     * @param string $key
     * @param mixed $value
     * @return bool
     * @throws Throwable
     */
    public function create(string $key, mixed $value): bool
    {
        $timestamp = CarbonImmutable::now()->timestamp;
        $serializedValue = json_encode($value, JSON_THROW_ON_ERROR);
        $this->redis->zadd("key:$key", $timestamp, $serializedValue);
        $this->redis->hmset('key_latest', [$key => $serializedValue]);
        return true;
    }

    /**
     * @throws JsonException
     */
    public function get(string $key, ?int $timestamp): mixed
    {
        $timestamp ??= '+inf';
        $values = $this->redis->zrevrangebyscore("key:$key", $timestamp, 0, [
            'WITHSCORES' => true,
            'LIMIT' => [0, 1],
        ]);
        if (!count($values)) {
            throw new NotFoundHttpException("$key not found");
        }
        $value = array_key_first($values);
        return json_decode($value, false, 512, JSON_THROW_ON_ERROR);
    }

    public function getAll(): Collection
    {
        $values = $this->redis->hGetAll('key_latest');
        return (new Collection($values))
            ->map(fn($value) => json_decode($value, false, 512, JSON_THROW_ON_ERROR));
    }
}
