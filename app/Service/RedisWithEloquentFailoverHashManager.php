<?php

declare(strict_types=1);

namespace App\Service;

use Illuminate\Redis\RedisManager;
use Illuminate\Support\Collection;
use JsonException;
use Psr\Log\LoggerInterface;
use Throwable;

class RedisWithEloquentFailoverHashManager implements HashManagerInterface
{
    public function __construct(
        private readonly RedisHashManager $redisManager,
        private readonly EloquentHashManager $eloquentHashManager,
        private readonly LoggerInterface $logger,
    ) {
    }

    /**
     * @param string $key
     * @param mixed $value
     * @return bool
     * @throws Throwable
     */
    public function create(string $key, mixed $value): bool
    {
        return $this->redisManager->create($key, $value);
    }

    /**
     * @param string $key
     * @param int|null $timestamp
     * @return mixed
     * @throws JsonException
     */
    public function get(string $key, ?int $timestamp): mixed
    {
        $value = $this->redisManager->get($key, $timestamp);
        if ($value === null) {
            $value = $this->eloquentHashManager->get($key, $timestamp);
            $this->logger->debug(sprintf('Query %s from database', $key));
        }

        return $value;
    }

    public function getAll(): Collection
    {
        $allValues = $this->redisManager->getAll();
        foreach ($allValues as $key => $value) {
            if ($value === null) {
                $allValues[$key] = $this->eloquentHashManager->get($key, null);
                $this->logger->debug(sprintf('Query %s from database', $key));
            }
        }
        return $allValues;
    }
}
