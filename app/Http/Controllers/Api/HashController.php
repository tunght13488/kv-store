<?php

namespace App\Http\Controllers\Api;

use App\Events\HashCreated;
use App\Http\Controllers\Controller;
use App\Service\HashManagerInterface;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use JsonException;
use Throwable;

class HashController extends Controller
{
    public function __construct(
        private readonly HashManagerInterface $hashManager,
        private readonly Dispatcher $dispatcher
    ) {
    }

    public function index(): JsonResponse
    {
        return new JsonResponse($this->hashManager->getAll());
    }

    /**
     * @param Request $request
     * @return Response
     * @throws Throwable
     */
    public function store(Request $request): Response
    {
        $hasStored = false;
        try {
            $this->validateJson($request);
            foreach ($request->input() as $key => $value) {
                $this->hashManager->create($key, $value);
                $this->dispatcher->dispatch(new HashCreated($key, $value));
                $hasStored = true;
            }
            $message = 'Nothing stored';
        } catch (JsonException) {
            $message = 'Invalid JSON';
        }

        return $hasStored
            ? new Response(null, 201)
            : new Response(['errors' => ['content' => $message]], 400);
    }

    public function show(Request $request, string $key): JsonResponse
    {
        return new JsonResponse($this->hashManager->get($key, $request->query('timestamp')), 200);
    }

    /**
     * @throws JsonException
     */
    private function validateJson(Request $request): void
    {
        if ($request->getContentType() === 'json') {
            $content = $request->getContent();
            json_decode($content, true, 512, JSON_THROW_ON_ERROR);
        }
    }
}
