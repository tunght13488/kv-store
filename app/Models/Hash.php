<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Casts\AsArrayObject;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @mixin IdeHelperHash
 */
class Hash extends Model
{
    use HasFactory;

    public $timestamps = false;
    protected $dateFormat = 'U';
    protected $hidden = [
        'id',
        'timestamp',
    ];

    public function scopeOfTimestamp(Builder $query, $timestamp): Builder
    {
        $query->orderByDesc('timestamp');
        if ($timestamp !== null) {
            $query->where('timestamp', '<=', $timestamp);
        }
        return $query;
    }

    protected function content(): Attribute
    {
        return Attribute::make(
            get: static fn($value) => json_decode($value, false, 512, JSON_THROW_ON_ERROR),
            set: static fn($value) => json_encode($value, JSON_THROW_ON_ERROR),
        );
    }
}
