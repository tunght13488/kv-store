<?php

namespace App\Listeners;

use App\Events\HashCreated;
use App\Service\EloquentHashManager;
use Throwable;

class SaveHash
{
    public function __construct(private readonly EloquentHashManager $eloquentHashManager)
    {
    }

    /**
     * @param HashCreated $event
     * @return void
     * @throws Throwable
     */
    public function handle(HashCreated $event): void
    {
        $this->eloquentHashManager->create($event->key, $event->value);
    }
}
