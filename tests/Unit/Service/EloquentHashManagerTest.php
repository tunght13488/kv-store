<?php

declare(strict_types=1);

namespace Tests\Unit\Service;

use App\Service\EloquentHashManager;
use Tests\TestCase;

/**
 * @covers \App\Service\EloquentHashManager
 */
class EloquentHashManagerTest extends TestCase
{
    private EloquentHashManager $subject;

    public function test_create(): void
    {
        $this->markTestIncomplete();
    }

    public function test_get(): void
    {
        $this->markTestIncomplete();
    }

    public function test_all(): void
    {
        $this->markTestIncomplete();
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->subject = new EloquentHashManager();
    }
}
