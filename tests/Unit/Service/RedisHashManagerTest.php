<?php

declare(strict_types=1);

namespace Tests\Unit\Service;

use App\Service\RedisHashManager;
use Illuminate\Contracts\Redis\Factory;
use Illuminate\Redis\Connections\Connection;
use Mockery;
use PHPUnit\Framework\TestCase;

/**
 * @covers \App\Service\RedisHashManager
 */
class RedisHashManagerTest extends TestCase
{
    private RedisHashManager $subject;
    private Mockery\LegacyMockInterface|Mockery\MockInterface|Connection $redisMock;

    public function test_create(): void
    {
        $this->markTestIncomplete();
    }

    public function test_get(): void
    {
        $this->markTestIncomplete();
    }

    public function test_all(): void
    {
        $this->markTestIncomplete();
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->redisMock = Mockery::mock(Connection::class);
        $factoryMock = Mockery::mock(Factory::class);
        $factoryMock->shouldReceive('connection')->withNoArgs()->andReturn($this->redisMock);
        $this->subject = new RedisHashManager(factory: $factoryMock);
    }
}
