<?php

declare(strict_types=1);

namespace Tests\Unit\Http\Controllers\Api;

use App\Events\HashCreated;
use App\Http\Controllers\Api\HashController;
use App\Service\HashManagerInterface;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Foundation\Testing\WithFaker;
use Mockery;
use Symfony\Component\HttpFoundation\Request;
use Tests\TestCase;

/**
 * @covers \App\Http\Controllers\Api\HashController
 */
class HashControllerTest extends TestCase
{
    use WithFaker;

    private HashController $subject;
    private Mockery\LegacyMockInterface|HashManagerInterface|Mockery\MockInterface $hashManagerMock;
    private Dispatcher|Mockery\LegacyMockInterface|Mockery\MockInterface $dispatcherMock;

    public function test_store_successfully(): void
    {
        $key = $this->faker->word;
        $value = [$this->faker->word => $this->faker->sentence];
        $request = $this->createRequest(
            '/object',
            method: Request::METHOD_POST,
            parameters: [
                $key => $value,
            ]
        );

        $this->hashManagerMock->shouldReceive('create')->with($key, $value);
        $this->dispatcherMock->shouldReceive('dispatch')->withArgs(function ($event) use ($value, $key) {
            return $event instanceof HashCreated
                && $event->key === $key
                && $event->value === $value;
        });

        $response = $this->subject->store($request);
        $this->assertEquals(201, $response->getStatusCode());
    }

    public function test_store_nothing(): void
    {
        $request = $this->createRequest(
            '/object',
            method: Request::METHOD_POST,
        );

        $response = $this->subject->store($request);
        $this->assertEquals(400, $response->getStatusCode());
        $this->hashManagerMock->shouldNotHaveReceived('create');
        $this->dispatcherMock->shouldNotHaveReceived('dispatch');
    }

    public function test_show(): void
    {
        $this->markTestIncomplete();
    }

    public function test_index(): void
    {
        $this->markTestIncomplete();
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->hashManagerMock = Mockery::mock(HashManagerInterface::class);
        $this->dispatcherMock = Mockery::mock(Dispatcher::class);
        $this->subject = new HashController(
            hashManager: $this->hashManagerMock,
            dispatcher: $this->dispatcherMock
        );
    }

    private function createRequest(
        string $uri,
        string $method = 'GET',
        array $parameters = [],
        array $cookies = [],
        array $files = [],
        array $server = [],
        $content = null
    ): \Illuminate\Http\Request {
        return \Illuminate\Http\Request::createFromBase(
            Request::create($uri, $method, $parameters, $cookies, $files, $server, $content)
        );
    }
}
