<?php

declare(strict_types=1);

namespace Tests\Feature;

use Carbon\CarbonImmutable;
use Illuminate\Contracts\Redis\Factory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Redis\Connections\Connection;
use Illuminate\Redis\Connections\PhpRedisConnection;
use JetBrains\PhpStorm\ArrayShape;
use Tests\TestCase;

class ObjectTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    /**
     * @var Connection&PhpRedisConnection
     */
    private Connection $redis;

    public function test_store_data_successfully(): void
    {
        ['key' => $key, 'value' => $value] = $this->generateInput();

        $this->createObject($key, $value);
    }

    public function test_submitting_empty_data(): void
    {
        $response = $this->postJson('/api/object', []);
        $response->assertStatus(400);
        $response->assertJsonValidationErrors(['content' => 'Nothing stored']);
    }

    public function test_submitting_malfunction_data(): void
    {
        $response = $this->call(
            'POST',
            '/api/object',
            server: $this->transformHeadersToServerVars([
                'CONTENT_TYPE' => 'application/json',
            ]),
            content: "{some invalid JSON"
        );
        $response->assertStatus(400);
        $response->assertJsonValidationErrors(['content' => 'Invalid JSON']);
    }

    public function test_retrieve_non_existing_key(): void
    {
        ['key' => $key] = $this->generateInput();

        $this->getObject($key, status: 404);
    }

    public function test_retrieve_data_successfully(): void
    {
        ['key' => $key, 'value' => $value] = $this->generateInput();

        $this->createObject($key, $value);

        $this->getObject($key, $value);
    }

    public function test_retrieve_versioned_data_correctly(): void
    {
        ['key' => $key, 'value' => $value1, 'timestamp' => $timestamp1] = $this->generateInput();
        $this->createObject($key, $value1, timestamp: $timestamp1);

        ['value' => $value2, 'timestamp' => $timestamp2] = $this->generateInput($timestamp1 + 1);
        $this->createObject($key, $value2, timestamp: $timestamp2);

        $this->getObject($key, $value2);

        ['timestamp' => $timestamp] = $this->generateInput($timestamp1 + 1, $timestamp2 - 1);
        $this->getObject($key, $value1, timestamp: $timestamp);
    }

    public function test_retrieve_data_falling_back_to_mysql(): void
    {
        ['key' => $key, 'value' => $value] = $this->generateInput();

        $this->createObject($key, $value);

        $this->deleteObject($key);

        $this->getObject($key, $value);
    }

    protected function setUp(): void
    {
        parent::setUp();
        $redisFactory = $this->app->get(Factory::class);
        $this->redis = $redisFactory->connection();
        $this->redis->flushdb();
    }

    #[ArrayShape(['key' => "string", 'value' => "array|string", 'timestamp' => "int"])]
    private function generateInput(int $min = 0, int $max = 2147483647): array
    {
        return [
            'key' => $this->faker->word,
            'value' => $this->faker->boolean ? $this->faker->sentence : [$this->faker->word => $this->faker->sentence],
            'timestamp' => $this->faker->numberBetween($min, $max),
        ];
    }

    private function createObject(string $key, mixed $value, int $status = 201, ?int $timestamp = null): void
    {
        if ($timestamp !== null) {
            CarbonImmutable::setTestNow(CarbonImmutable::createFromTimestamp($timestamp));
        }
        $response = $this->postJson('/api/object', [$key => $value]);
        $response->assertStatus($status);
    }

    private function getObject(string $key, mixed $value = null, ?int $status = 200, int $timestamp = null): void
    {
        $uri = $timestamp === null ? "/api/object/$key" : "/api/object/$key?timestamp=$timestamp";
        $response = $this->getJson($uri);
        $response->assertStatus($status);
        if (is_string($value)) {
            $this->assertEquals($value, $response->json());
        }
        if (is_array($value)) {
            $response->assertJson($value, true);
        }
    }

    private function deleteObject(string $key): void
    {
        $this->redis->del($key);
    }
}
