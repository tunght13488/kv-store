MAKEFILE_PATH := $(abspath $(lastword $(MAKEFILE_LIST)))
ROOT_DIR := $(patsubst %/,%,$(dir $(MAKEFILE_PATH)))

# Folders
STORAGE_DIR = $(ROOT_DIR)/storage
BOOTSTRAP_CACHE_DIR = $(ROOT_DIR)/bootstrap/cache

# Executable
EXECUTABLES = php npm setfacl composer
K := $(foreach exec,$(EXECUTABLES),\
        $(if $(shell which $(exec)),some string,$(error "No $(exec) in PATH")))
PHP = /usr/bin/env php
NPM = /usr/bin/env npm
SET_FACL = /usr/bin/env setfacl
COMPOSER = /usr/bin/env composer
ifeq (, $(shell which phive 2>/dev/null))
$(error "No phive in $(PATH), see https://phar.io for installation details")
endif
PHIVE = /usr/bin/env phive
ARTISAN = $(ROOT_DIR)/artisan
PHP_ARTISAN = $(PHP) $(ARTISAN)

# Utils
HTTPDUSER = `ps axo user,comm | grep -E '[a]pache|[h]ttpd|[_]www|[w]ww-data|[n]ginx' | grep -v root | head -1 | cut -d\  -f1`
ME = `whoami`

default: dev

dev: composer-install npm-install npm-compile clear-cache ide-helper phpcpd lint

prod: composer-install-prod npm-install-prod npm-compile-prod clear-cache

deploy: maintenance-on prod maintenance-off

permission: set-permission-linux

composer-install:
	$(COMPOSER) install --no-progress

composer-install-prod:
	$(COMPOSER) install --no-progress --no-dev

set-permission-mac:
	sudo chmod +a "$(HTTPDUSER) allow delete,write,append,file_inherit,directory_inherit" $(STORAGE_DIR) $(BOOTSTRAP_CACHE_DIR)
	sudo chmod +a "$(ME) allow delete,write,append,file_inherit,directory_inherit" $(STORAGE_DIR) $(BOOTSTRAP_CACHE_DIR)

set-permission-linux:
	sudo $(SET_FACL) -R -m u:$(HTTPDUSER):rwX -m u:$(ME):rwX $(STORAGE_DIR) $(BOOTSTRAP_CACHE_DIR)
	sudo $(SET_FACL) -dR -m u:$(HTTPDUSER):rwX -m u:$(ME):rwX $(STORAGE_DIR) $(BOOTSTRAP_CACHE_DIR)

npm-install:
	$(NPM) install

npm-install-prod:
	$(NPM) install --production

npm-compile:
	$(NPM) run dev

npm-compile-prod:
	$(NPM) run prod

maintenance-on:
	$(PHP_ARTISAN) down

maintenance-off:
	$(PHP_ARTISAN) up

ide-helper:
	$(PHP_ARTISAN) ide-helper:generate
	$(PHP_ARTISAN) ide-helper:meta
	$(PHP_ARTISAN) ide-helper:models -M

clear-cache:
	$(PHP_ARTISAN) cache:clear
	$(PHP_ARTISAN) clear-compiled

phpcpd:
	$(PHIVE) install phpcpd
	./tools/phpcpd app/ --min-lines=50

lint:
	$(COMPOSER) run lint

security-check:
	@test -f local-php-security-checker || wget https://github.com/fabpot/local-php-security-checker/releases/download/v1.0.0/local-php-security-checker_1.0.0_linux_amd64 --no-verbose --output-document local-php-security-checker && chmod +x local-php-security-checker
	@./local-php-security-checker composer.lock

.PHONY: default dev prod deploy composer-install composer-install-prod set-permission-mac set-permission-linux permission check check-setfacl check-composer check-NPM npm-install npm-install-prod maintenance-on maintenance-off check-phive security-check
