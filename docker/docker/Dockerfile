FROM docker:18.09.7 as base-docker

RUN apk add --update \
    docker \
    openrc \
    ca-certificates \
    wget \
    && rm -rf /var/cache/apt/*
RUN rc-update add docker boot

# RUN apk add wget curl jq py-pip bash git openssh curl sed
# RUN apk add --no-cache ca-certificates

ENV KUBECTL_VERSION=1.24.1
RUN wget -q "https://dl.k8s.io/release/v${KUBECTL_VERSION}/bin/linux/amd64/kubectl" \
    -O /usr/local/bin/kubectl \
    && chmod +x /usr/local/bin/kubectl \
    && echo " ==> kubectl installed"

ENV HELM_VERSION=3.9.0
RUN wget -q "https://get.helm.sh/helm-v${HELM_VERSION}-linux-amd64.tar.gz" \
    -O - | tar -xzO linux-amd64/helm > /usr/local/bin/helm \
    && chmod +x /usr/local/bin/helm \
    && echo " ==> helm installed"

ENV DOCTL_VERSION=1.76.2
RUN wget -q "https://github.com/digitalocean/doctl/releases/download/v${DOCTL_VERSION}/doctl-${DOCTL_VERSION}-linux-amd64.tar.gz" \
    -O - | tar -xzO doctl > /usr/local/bin/doctl \
    && chmod +x /usr/local/bin/doctl \
    && echo " ==> doctl installed"
