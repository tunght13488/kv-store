#!/usr/bin/env bash

set -Eeuo pipefail

if [ "$1" = 'nginx' ]; then
    # Check nginx syntax
    nginx -t
fi

/usr/local/bin/dumb-init -- "$@"
