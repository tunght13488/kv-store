#syntax=docker/dockerfile:experimental


# ******************
# *** Nginx Base ***
# ******************
FROM nginx:1.20 as base-nginx

RUN apt-get update  \
    && apt-get upgrade -y \
    && apt-get install -y \
    wget \
    unzip \
    && rm -rf /var/lib/apt/lists/* \
    && echo " ==> Common Software Installed"

ENV DUMP_INIT_VERSION=1.2.5
RUN wget -q "https://github.com/Yelp/dumb-init/releases/download/v${DUMP_INIT_VERSION}/dumb-init_${DUMP_INIT_VERSION}_x86_64" \
    -O /usr/local/bin/dumb-init \
    && chmod +x /usr/local/bin/dumb-init \
    && echo " ==> dump-init Installed"

COPY bin/docker-entrypoint.sh /docker-entrypoint.sh
RUN chmod +x /docker-entrypoint.sh \
    && echo " ==> docker-entrypoint.sh Prepared"

ENV APP_DIR=/app
RUN mkdir -p "${APP_DIR}" \
    && echo " ==> ${APP_DIR} Directories Created"

WORKDIR ${APP_DIR}

COPY etc/conf.d/upstream.conf /etc/nginx/conf.d/upstream.conf
COPY etc/conf.d/default.conf /etc/nginx/conf.d/default.conf
COPY etc/nginx.conf /etc/nginx/nginx.conf

EXPOSE 80 443

ENTRYPOINT ["/docker-entrypoint.sh"]

CMD ["nginx", "-g", "daemon off;"]

