#!/usr/bin/env bash

set -Eeuo pipefail

[[ -n "${PATH_DOTENV:-}" ]] && cp "${PATH_DOTENV}" "${APP_DIR}"/.env

if [ "$1" = 'php-fpm' ]; then
    [[ -n "${PM_MAX_CHILDREN:-}" ]] && sed -i "s/!PM_MAX_CHILDREN!/${PM_MAX_CHILDREN}/" /usr/local/etc/php-fpm.d/zz-custom.conf
    [[ -n "${PM_START_SERVERS:-}" ]] && sed -i "s/!PM_START_SERVERS!/${PM_START_SERVERS}/" /usr/local/etc/php-fpm.d/zz-custom.conf
    [[ -n "${PM_MIN_SPARE_SERVERS:-}" ]] && sed -i "s/!PM_MIN_SPARE_SERVERS!/${PM_MIN_SPARE_SERVERS}/" /usr/local/etc/php-fpm.d/zz-custom.conf
    [[ -n "${PM_MAX_SPARE_SERVERS:-}" ]] && sed -i "s/!PM_MAX_SPARE_SERVERS!/${PM_MAX_SPARE_SERVERS}/" /usr/local/etc/php-fpm.d/zz-custom.conf
    [[ -n "${PM_MAX_REQUESTS:-}" ]] && sed -i "s/!PM_MAX_REQUESTS!/${PM_MAX_REQUESTS}/" /usr/local/etc/php-fpm.d/zz-custom.conf

    php-fpm -t
fi

/usr/local/bin/dumb-init -- "$@"
