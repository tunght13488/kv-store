#!/usr/bin/env bash

set -Eeuo pipefail

[[ -n "${PATH_DOTENV:-}" ]] && cp "${PATH_DOTENV}" "${APP_DIR}"/.env

/usr/local/bin/dumb-init -- "$@"
