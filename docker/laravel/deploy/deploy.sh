#!/usr/bin/env bash

set -Eeuo pipefail

export COMMON_ARGS="--ansi --no-interaction"

php $APP_DIR/artisan view:cache $COMMON_ARGS

php $APP_DIR/artisan optimize $COMMON_ARGS

cat $APP_DIR/.env

php $APP_DIR/artisan migrate --force $COMMON_ARGS
