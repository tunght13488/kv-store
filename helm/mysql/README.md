# Gitlab Runner

## Configure

```shell
helm repo add bitnami https://charts.bitnami.com/bitnami
```

Copy `values.example.yaml` to `values.yaml`, then update it with the real credentials

## Install

```shell
helm install \
    --namespace mysql \
    --create-namespace \
    --values values.yaml \
    mysql \
    bitnami/mysql
```

## Upgrade

```shell
helm upgrade \
    --namespace mysql \
    --values values.yaml \
    mysql \
    bitnami/mysql
```

## Delete

```shell
helm delete \
    --namespace mysql \
    mysql
```
