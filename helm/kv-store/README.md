# KV Store

## Test

```shell
helm install \
    --dry-run \
    --debug \
    --namespace kv-store \
    --create-namespace \
    --values values.yaml \
    kv-store \
    .
```

## Install

```shell
helm install \
    --namespace kv-store \
    --create-namespace \
    --values values.yaml \
    kv-store \
    .
```

## Upgrade

```shell
helm upgrade \
    --namespace kv-store \
    --values values.yaml \
    kv-store \
    .
```

## Delete

```shell
helm delete \
    --namespace kv-store \
    kv-store
```
