# Gitlab Runner

## Configure

```shell
helm repo add gitlab https://charts.gitlab.io
```

Copy `values.example.yaml` to `values.yaml`, then update it with the real credentials

## Install

```shell
helm install \
    --namespace gitlab-runner \
    --create-namespace \
    --values values.yaml \
    gitlab-runner \
    gitlab/gitlab-runner
```

## Upgrade

```shell
helm upgrade \
    --namespace gitlab-runner \
    --values values.yaml \
    gitlab-runner \
    gitlab/gitlab-runner
```

## Delete

```shell
helm delete \
    --namespace gitlab-runner \
    gitlab-runner
```
