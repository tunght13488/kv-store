# Gitlab Runner

## Configure

```shell
helm repo add bitnami https://charts.bitnami.com/bitnami
```

Copy `values.example.yaml` to `values.yaml`, then update it with the real credentials

## Install

```shell
helm install \
    --namespace redis \
    --create-namespace \
    --values values.yaml \
    redis \
    bitnami/redis
```

## Upgrade

```shell
helm upgrade \
    --namespace redis \
    --values values.yaml \
    redis \
    bitnami/redis
```

## Delete

```shell
helm delete \
    --namespace redis \
    redis
```
