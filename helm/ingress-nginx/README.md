# Gitlab Runner

## Configure

```shell
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
```

Copy `values.example.yaml` to `values.yaml`, then update it with the real credentials

## Install

```shell
helm install \
  --namespace kv-store \
  --create-namespace \
  --values values.yaml \
  nginx-ingress \
  ingress-nginx/ingress-nginx
```

## Upgrade

```shell
helm upgrade \
  --namespace kv-store \
  --values values.yaml \
  nginx-ingress \
  ingress-nginx/ingress-nginx
```

## Delete

```shell
helm delete \
  --namespace kv-store \
  nginx-ingress
```
