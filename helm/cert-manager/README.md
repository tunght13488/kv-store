# Gitlab Runner

## Configure

```shell
helm repo add jetstack https://charts.jetstack.io
```

Copy `values.example.yaml` to `values.yaml`, then update it with the real credentials

## Install

```shell
helm install \
  --namespace cert-manager \
  --create-namespace \
  --values values.yaml \
  cert-manager \
  jetstack/cert-manager
```

## Upgrade

```shell
helm upgrade \
  --namespace cert-manager \
  --values values.yaml \
  cert-manager \
  jetstack/cert-manager
```

## Delete

```shell
helm delete \
  --namespace cert-manager \
  cert-manager
```
