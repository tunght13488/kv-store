<?php

use App\Http\Controllers\Api\HashController;
use App\Http\Controllers\Api\ObjectController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('object')->group(function () {
    Route::post('/', [HashController::class, 'store']);
    Route::get('/get_all_records', [HashController::class, 'index']);
    Route::get('/{key}', [HashController::class, 'show']);
});
