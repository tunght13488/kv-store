
[![pipeline status](https://gitlab.com/tunght13488/kv-store/badges/master/pipeline.svg)](https://gitlab.com/tunght13488/kv-store/-/commits/master)
[![coverage report](https://gitlab.com/tunght13488/kv-store/badges/master/coverage.svg)](https://gitlab.com/tunght13488/kv-store/-/commits/master)
[![Latest Release](https://gitlab.com/tunght13488/kv-store/-/badges/release.svg)](https://gitlab.com/tunght13488/kv-store/-/releases)

# Key Value Store

## Getting Started

- Get a DigitalOcean K8s cluster
- Deploy Gitlab Runner (helm/gitlab-runner/README.md)
- Deploy MySQL (helm/mysql/README.md)
- Deploy Redis (helm/redis/README.md)
- Deploy Nginx Ingress (helm/ingress-redis/README.md)
- Update ENV variable for gitlab
- Trigger deployment from pipeline

## Contribution

Install git hooks

```shell
# Activate git hooks
npx husky install
```

## Todo

- [x] Feature Test
- [ ] Unit Test
- [x] CI/CD pipeline
  - [x] Docker build jobs
  - [x] Helm plan/deploy jobs
  - [x] Unit test Job with test report & coverage report
  - [ ] Lint job
