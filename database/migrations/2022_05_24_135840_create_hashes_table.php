<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::create('hashes', static function (Blueprint $table) {
            $table->id();
            $table->string('identifier');
            $table->json('content');
            $table->unsignedInteger('timestamp');
            $table->index(['identifier', 'timestamp']);
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('hashes');
    }
};
