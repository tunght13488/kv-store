#syntax=docker/dockerfile:experimental


ARG BASE_PHP_CLI=base-php-cli
ARG BASE_PHP_FPM=base-php-fpm

ARG COMPOSER_VERSION=2.3.5

FROM composer:${COMPOSER_VERSION} as vanilla-composer

# ************************
# *** Laravel Composer ***
# ************************
FROM ${BASE_PHP_CLI} as laravel-composer

COPY --from=vanilla-composer /usr/bin/composer /usr/bin/composer
RUN composer --version \
    && echo " ==> Composer Installed"

COPY composer.json composer.json
COPY composer.lock composer.lock

RUN composer install \
    --prefer-dist \
    --no-dev \
    --no-progress \
    --ansi \
    --no-interaction \
    --no-plugins \
    --no-scripts \
    && echo " ==> Composer Dependencies Installed"

# ********************
# *** Laravel Code ***
# ********************
FROM ${BASE_PHP_CLI} as laravel-code

COPY . .

COPY --from=vanilla-composer /usr/bin/composer /usr/bin/composer
RUN composer --version \
    && echo " ==> Composer Installed"

COPY --from=laravel-composer ${APP_DIR}/vendor ${APP_DIR}/vendor

RUN composer dump-autoload \
    --optimize \
    --no-dev \
    --ansi \
    --no-interaction
